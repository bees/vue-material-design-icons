const fs = require('fs')
const mustache = require('mustache')
const path = require('path')

const dist = path.resolve(__dirname, 'dist')
const template = path.resolve(__dirname, 'template.mst')
const svgPath = path.resolve(__dirname, 'MaterialDesign-SVG/svg')

const svgs = fs.readdirSync(svgPath)

const getPath = (svg) => {
  const matches = /\sd="(.*)"/.exec(fs.readFileSync(path.join(svgPath, svg), {
    encoding: 'utf8'
  }))

  if (matches) {
    return matches[0]
  }
}

const makeHumanReadable = (name) => {
  let spacedName = name.split('-').join(' ')
  humanReadableName = spacedName.charAt(0).toUpperCase() + spacedName.slice(1)
  return humanReadableName
}

const makeComponentName = (name) => {
  let lowerCamelCase = name.replace(/-([a-z0-9])/g, g => g[1].toUpperCase())
  let upperCamelCase = lowerCamelCase.charAt(0).toUpperCase() + lowerCamelCase.slice(1)
  return upperCamelCase
}

let templateData = svgs.map(svg => {
  let name = svg.slice(0, -4)
  let componentName = makeComponentName(name)
  return {
    name: componentName,
    readableName: makeHumanReadable(name),
    path: getPath(svg)
  }
})

let componentFile = fs.readFileSync(template, { encoding: 'utf8'})

if (!fs.existsSync(dist)) {
  fs.mkdirSync(dist)
}

let templateFile = fs.readFileSync(template, { encoding: 'utf8'})
fs.writeFileSync(path.resolve(dist, 'index.js'), mustache.render(templateFile, {templates: templateData}))
